package net.pl3x.bukkit.mcmmorankrewards;

import net.pl3x.bukkit.mcmmorankrewards.configuration.Config;
import net.pl3x.bukkit.mcmmorankrewards.hook.Pl3xSignsHook;
import net.pl3x.bukkit.mcmmorankrewards.hook.VaultHook;
import net.pl3x.bukkit.mcmmorankrewards.listener.ScoreboardListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

public class McMMORankRewards extends JavaPlugin {
    private BukkitTask updateTask;
    private Ranks ranks;
    private boolean hasPl3xIcons = false;

    @Override
    public void onEnable() {
        Config.reload();

        if (failedDependencyCheck()) {
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }

        ranks = new Ranks();

        updateTask = Bukkit.getScheduler().runTaskTimerAsynchronously(this, ranks::updateRanks, 20, 12000);

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        if (updateTask != null) {
            updateTask.cancel();
            updateTask = null;
        }

        Logger.info(getName() + " disabled.");
    }

    private boolean failedDependencyCheck() {
        if (!Bukkit.getPluginManager().isPluginEnabled("mcMMO")) {
            Logger.error("# McMMO NOT found and/or enabled!");
            Logger.error("# This plugin requires McMMO to be installed and enabled!");
            return true; // failed
        }
        if (!Bukkit.getPluginManager().isPluginEnabled("Vault")
                || VaultHook.failedSetupPermissions()
                || VaultHook.failedSetupChat()) {
            Logger.error("# Vault NOT found and/or enabled!");
            Logger.error("# This plugin requires Vault to be installed and enabled!");
            return true; // failed
        }
        if (Bukkit.getPluginManager().isPluginEnabled("Pl3xIcons")) {
            Logger.info("Pl3xIcons found. Registering icon translator...");
            hasPl3xIcons = true;
        }
        if (Bukkit.getPluginManager().isPluginEnabled("Pl3xSigns")) {
            Logger.info("Pl3xSigns found. Registering sign packet listener...");
            Bukkit.getPluginManager().registerEvents(new Pl3xSignsHook(this), this);
        }
        if (Bukkit.getPluginManager().isPluginEnabled("ProtocolLib")) {
            Logger.info("ProtocolLib found. Registering packet listener...");
            new ScoreboardListener(this);
        }
        return false; // passed
    }

    public static McMMORankRewards getPlugin() {
        return McMMORankRewards.getPlugin(McMMORankRewards.class);
    }

    public Ranks getRanksManager() {
        return ranks;
    }

    public boolean hasPl3xIcons() {
        return hasPl3xIcons;
    }
}
