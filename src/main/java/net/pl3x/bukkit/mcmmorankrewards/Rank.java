package net.pl3x.bukkit.mcmmorankrewards;

import net.milkbowl.vault.permission.Permission;
import net.pl3x.bukkit.mcmmorankrewards.configuration.Config;
import net.pl3x.bukkit.mcmmorankrewards.hook.Pl3xIconsHook;
import net.pl3x.bukkit.mcmmorankrewards.hook.VaultHook;
import org.apache.commons.lang.WordUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;

public class Rank {
    private final RankType type;
    private final String group;
    private final String[] signLines = new String[4];
    private OfflinePlayer leader;
    private OfflinePlayer[] top20 = new OfflinePlayer[20];

    public Rank(RankType type, String group) {
        this.type = type;
        this.group = group;
        // do not set leader here. we dont know if they are leader of higher priority rank or not

        signLines[0] = WordUtils.capitalizeFully(type.name());
        signLines[1] = "&oNo one";
        signLines[2] = "Lvl: 0";

        String prefix = VaultHook.getChat().getGroupPrefix(Bukkit.getWorlds().get(0), getGroup());
        if (McMMORankRewards.getPlugin().hasPl3xIcons()) {
            String newPrefix = Pl3xIconsHook.translate(prefix);
            if (!prefix.equals(newPrefix)) {
                prefix = "&f" + newPrefix + "&r";
            }
        }
        signLines[3] = prefix + getGroup();
    }

    public RankType getType() {
        return type;
    }

    public String getGroup() {
        return group;
    }

    public String[] getSignLines() {
        return signLines;
    }

    public void updateSignLines() {
        signLines[1] = leader == null ? "&oNo one" : leader.getName();
        signLines[2] = "Lvl: " + (leader == null ? 0 : RankHelper.getLevel(leader.getUniqueId(), type));
    }

    public OfflinePlayer getLeader() {
        return leader;
    }

    public boolean isLeader(OfflinePlayer player) {
        return leader != null && player != null && leader.getUniqueId().equals(player.getUniqueId());
    }

    public OfflinePlayer[] getTop20() {
        return top20;
    }

    public void updateTop20() {
        this.top20 = getType().getTop20();
    }

    public void setLeader(OfflinePlayer leader) {
        this.leader = leader;

        updateSignLines();

        Permission perm = VaultHook.getPermission();

        // get back on the main thread to add new leader perms
        if (leader != null) {
            Bukkit.getScheduler().runTaskLater(McMMORankRewards.getPlugin(), () -> {
                perm.playerAddGroup(null, getLeader(), getGroup());
                if (Config.ANNOUNCE_LEADER_CHANGE != null && !Config.ANNOUNCE_LEADER_CHANGE.isEmpty()) {
                    Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&',
                            Config.ANNOUNCE_LEADER_CHANGE
                                    .replace("{player}", getLeader().getName())
                                    .replace("{skill}", WordUtils.capitalize(getType().name().toLowerCase()))
                                    .replace("{group}", getGroup())));
                }
            }, 1);
        }

        // remove all old leaders from this group
        Bukkit.getScheduler().runTaskAsynchronously(McMMORankRewards.getPlugin(), () -> {
            for (OfflinePlayer offline : Bukkit.getOfflinePlayers()) {
                if (offline.equals(leader)) {
                    continue; // do not remove self
                }

                // get back on the main thread
                Bukkit.getScheduler().runTaskLater(McMMORankRewards.getPlugin(), () -> {
                    if (perm.playerInGroup(null, offline, group)) {
                        perm.playerRemoveGroup(null, offline, group);
                    }
                }, 1);
            }
        });
    }
}
