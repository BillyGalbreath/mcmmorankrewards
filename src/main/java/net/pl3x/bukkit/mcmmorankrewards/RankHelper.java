package net.pl3x.bukkit.mcmmorankrewards;

import com.gmail.nossr50.api.ExperienceAPI;
import com.gmail.nossr50.api.exceptions.InvalidPlayerException;
import com.gmail.nossr50.datatypes.database.PlayerStat;
import com.gmail.nossr50.mcMMO;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class RankHelper {
    public static List<PlayerStat> getTop(RankType type, int number) {
        if (type == RankType.SALVAGE || type == RankType.SMELTING) {
            List<PlayerStat> top = new ArrayList<>();
            for (OfflinePlayer offline : Bukkit.getOfflinePlayers()) {
                try {
                    top.add(new PlayerStat(offline.getName(), getLevel(offline.getUniqueId(), type)));
                } catch (InvalidPlayerException ignore) {
                }
            }
            Collections.sort(top, new SkillComparator());
            return top.subList(0, Math.min(top.size(), number));
        }
        return mcMMO.getDatabaseManager().readLeaderboard(type.getSkillType(), 1, number);
    }

    public static int getLevel(UUID uuid, RankType type) {
        switch (type) {
            case POWER:
                return ExperienceAPI.getPowerLevelOffline(uuid);
            case SALVAGE:
                return (ExperienceAPI.getLevelOffline(uuid, "mining")
                        + ExperienceAPI.getLevelOffline(uuid, "fishing")) / 2;
            case SMELTING:
                return (ExperienceAPI.getLevelOffline(uuid, "mining")
                        + ExperienceAPI.getLevelOffline(uuid, "repair")) / 2;
            default:
                return ExperienceAPI.getLevelOffline(uuid, type.name().toLowerCase());
        }
    }
}
