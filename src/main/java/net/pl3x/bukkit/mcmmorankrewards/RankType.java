package net.pl3x.bukkit.mcmmorankrewards;

import com.gmail.nossr50.datatypes.database.PlayerStat;
import com.gmail.nossr50.datatypes.skills.SkillType;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import java.util.List;

public enum RankType {
    ACROBATICS(SkillType.ACROBATICS),
    ALCHEMY(SkillType.ALCHEMY),
    ARCHERY(SkillType.ARCHERY),
    AXES(SkillType.AXES),
    EXCAVATION(SkillType.EXCAVATION),
    FISHING(SkillType.FISHING),
    HERBALISM(SkillType.HERBALISM),
    MINING(SkillType.MINING),
    POWER(null), // total sum of all skill levels
    REPAIR(SkillType.REPAIR),
    SALVAGE(SkillType.SALVAGE), // child skill (fishing + repair)
    SMELTING(SkillType.SMELTING), // child skill (mining + repair)
    SWORDS(SkillType.SWORDS),
    TAMING(SkillType.TAMING),
    UNARMED(SkillType.UNARMED),
    WOODCUTTING(SkillType.WOODCUTTING);

    private SkillType skillType;

    RankType(SkillType skillType) {
        this.skillType = skillType;
    }

    public SkillType getSkillType() {
        return skillType;
    }

    public OfflinePlayer[] getTop20() {
        OfflinePlayer[] top20 = new OfflinePlayer[20];

        List<PlayerStat> leaders = RankHelper.getTop(this, 50);
        if (leaders == null || leaders.isEmpty()) {
            Logger.debug("No leader found for skill: " + name());
            return top20;
        }

        int count = 0;
        for (int i = 0; i < leaders.size() && count < 20; i++) {
            OfflinePlayer leader = Bukkit.getOfflinePlayer(leaders.get(i).name);
            if (leader == null) {
                Logger.warn("Unable to get OfflinePlayer by name: " + leaders.get(i).name);
                continue;
            }
            top20[count] = leader;
            count++;
        }

        return top20;
    }
}
