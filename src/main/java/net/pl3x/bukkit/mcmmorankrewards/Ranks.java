package net.pl3x.bukkit.mcmmorankrewards;

import net.pl3x.bukkit.mcmmorankrewards.configuration.Config;
import net.pl3x.bukkit.mcmmorankrewards.hook.Pl3xIconsHook;
import net.pl3x.bukkit.mcmmorankrewards.hook.VaultHook;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

public class Ranks {
    private final List<Rank> ranks = new ArrayList<>();

    public Ranks() {
        List<String> allGroups = Arrays.asList(VaultHook.getPermission().getGroups());
        List<Map<?, ?>> allRanks = Config.getConfig().getMapList("ranks");

        for (Map<?, ?> rankEntry : allRanks) {
            for (Map.Entry entry : rankEntry.entrySet()) {
                String rankName = entry.getKey().toString();
                String rankGroup = entry.getValue().toString();

                if (!allGroups.contains(rankGroup)) {
                    Logger.error("Group is not setup in permissions plugin! (" + rankName + ":" + rankGroup + ")");
                    continue;
                }

                RankType rankType;
                try {
                    rankType = RankType.valueOf(rankName.toUpperCase());
                } catch (IllegalArgumentException e) {
                    Logger.error("Invalid rank type in config! (" + rankName + ")");
                    continue;
                }

                Logger.debug("Registered rank " + rankName + " for group " + rankGroup);
                ranks.add(new Rank(rankType, rankGroup));
            }
        }
    }

    public void updateRanks() {
        Logger.debug("Updating McMMO Ranks");

        Map<Rank, OfflinePlayer> newLeaders = new HashMap<>();

        ranks.forEach(Rank::updateTop20);

        // set leader of power; no contest here, top player gets it
        Rank power = getRank(RankType.POWER);
        newLeaders.put(power, power.getTop20()[0]);

        int counter = 0;
        while (newLeaders.size() < ranks.size()) {
            Logger.debug("#############################################");
            Logger.debug("Update run cycle: " + counter);
            if (counter > 50) {
                Logger.warn("Update cycle seems to be stuck in a loop (cycled more than 50 times).");
                Logger.warn("This can happen if ranks do not have clear winners.");
                break; // failsafe
            }
            counter++;

            // grab top choice for each rank
            Map<Rank, OfflinePlayer> firstChoicePicks = new HashMap<>();
            for (Rank rank : ranks) {
                if (newLeaders.containsKey(rank)) {
                    continue; // do not care about ranks that have a clear choice leader already
                }

                // cycle the top 20 for this rank to get a choice pick
                OfflinePlayer[] top20 = rank.getTop20();
                for (OfflinePlayer player : top20) {
                    if (player == null) {
                        continue;
                    }

                    if (newLeaders.containsValue(player)) {
                        continue; // dont choose player if already a clear choice leader of another rank
                    }

                    // top player is choice pick
                    firstChoicePicks.put(rank, player);
                    break; // stop looping players
                }
            }

            // DEBUG: show us unaltered 1st choice picks
            if (Config.DEBUG_MODE) {
                Logger.debug("######### First choice picks:");
                firstChoicePicks.entrySet()
                        .forEach(entry -> Logger.debug("     " + entry.getKey().getType().name() + ": " + entry.getValue().getName() + " - " + RankHelper.getLevel(entry.getValue().getUniqueId(), entry.getKey().getType())));
            }

            // look for clear 1st choice winners (choice picks that are picked for only ONE rank)
            Map<Rank, OfflinePlayer> firstChoiceWinners = new HashMap<>();
            Map<Rank, OfflinePlayer> secondChoicePicks = new HashMap<>();
            Set<OfflinePlayer> alreadyCheckedFirst = new HashSet<>();
            for (OfflinePlayer choicePick : new HashSet<>(firstChoicePicks.values())) {
                // check if player was already checked
                if (alreadyCheckedFirst.contains(choicePick)) {
                    continue; // choice pick already checked
                }

                // mark player as already checked
                alreadyCheckedFirst.add(choicePick);

                // check if choice pick is a clear winner
                if (Collections.frequency(firstChoicePicks.values(), choicePick) == 1) {
                    // set to newLeaders
                    firstChoicePicks.entrySet().stream()
                            .filter(entry -> entry.getValue().equals(choicePick))
                            .forEach(entry -> {
                                newLeaders.put(entry.getKey(), choicePick);
                                firstChoiceWinners.put(entry.getKey(), choicePick);
                            });

                    continue; // check next choice pick
                }

                // choice pick is picked multiple times; add to second choice pick map
                firstChoicePicks.entrySet().stream()
                        .filter(entry -> entry.getValue().equals(choicePick))
                        .forEach(entry -> secondChoicePicks.put(entry.getKey(), choicePick));
            }

            // show and assign 1st choice winners picked for multiple ranks
            Logger.debug("######### First choice winners:");
            firstChoiceWinners.entrySet()
                    .forEach(entry -> {
                        if (Config.DEBUG_MODE) {
                            Logger.debug("     " + entry.getKey().getType().name() + ": " + entry.getValue().getName() + " - " + RankHelper.getLevel(entry.getValue().getUniqueId(), entry.getKey().getType()));
                        }
                        // assign clear winner to newLeaders
                        newLeaders.put(entry.getKey(), entry.getValue());
                    });

            // DEBUG: show 2nd choice picks
            if (Config.DEBUG_MODE) {
                Logger.debug("######### Second choice picks:");
                secondChoicePicks.entrySet()
                        .forEach(entry -> Logger.debug("     " + entry.getKey().getType().name() + ": " + entry.getValue().getName() + " - " + RankHelper.getLevel(entry.getValue().getUniqueId(), entry.getKey().getType())));
            }

            // check each choice pick and compare the rank scores for which they were picked and assign to highest scored rank
            Map<Rank, OfflinePlayer> secondChoiceWinners = new HashMap<>();
            for (Rank rank : ranks) {
                if (newLeaders.containsKey(rank)) {
                    continue;
                }

                OfflinePlayer choicePick = secondChoicePicks.get(rank);

                // setup the holder for scores with sorting
                SortedMap<Integer, Rank> scores = new TreeMap<>(Collections.reverseOrder());

                // get the choice pick's scores for each picked rank
                secondChoicePicks.entrySet().stream()
                        .filter(entry -> entry.getValue().equals(choicePick))
                        .forEach(entry -> scores.put(
                                RankHelper.getLevel(choicePick.getUniqueId(), entry.getKey().getType()),
                                entry.getKey()));

                if (!scores.isEmpty()) {
                    secondChoiceWinners.put(scores.get(scores.firstKey()), choicePick);
                }
                break;
            }

            // show and assign 1st choice winners picked for multiple ranks
            Logger.debug("######### Second choice winners:");
            secondChoiceWinners.entrySet()
                    .forEach(entry -> {
                        if (Config.DEBUG_MODE) {
                            Logger.debug("     " + entry.getKey().getType().name() + ": " + entry.getValue().getName() + " - " + RankHelper.getLevel(entry.getValue().getUniqueId(), entry.getKey().getType()));
                        }
                        // assign clear winner to newLeaders
                        newLeaders.put(entry.getKey(), entry.getValue());
                    });
        }

        // DEBUG: show final newLeaders map before iterating for actual assignment
        if (Config.DEBUG_MODE) {
            Logger.debug("#############################################");
            Logger.debug("######### Final leader results:");
            newLeaders.entrySet()
                    .forEach(entry -> Logger.debug("     " + entry.getKey().getType().name() + ": " + entry.getValue().getName() + " - " + RankHelper.getLevel(entry.getValue().getUniqueId(), entry.getKey().getType())));
        }

        // check each rank and make sure winner's score is NOT zero; remove winner if score is zero
        newLeaders.entrySet().removeIf(entry -> RankHelper.getLevel(entry.getValue().getUniqueId(), entry.getKey().getType()) == 0);

        // DEBUG: show any groups that do not have a clear winner
        if (Config.DEBUG_MODE) {
            Logger.debug("######### Ranks without leaders:");
            ranks.stream()
                    .filter(rank -> !newLeaders.containsKey(rank))
                    .forEach(rank -> Logger.debug("    " + rank.getType().name()));
        }

        Logger.debug("#############################################");

        // iterate newLeaders and assign to their group
        for (Rank rank : ranks) {
            // make sure a leader is set (new servers might not have a clear winner for each group)
            OfflinePlayer leader = newLeaders.get(rank);
            if (leader == null) {
                Logger.warn("There is no leader for " + rank.getType().name());
                rank.setLeader(null);
                continue;
            }

            // make sure the leader is NOT already set; we dont want to re-assign someone to the SAME rank (which causes in-game broadcast spam)
            if (rank.getLeader() == null || !rank.getLeader().getUniqueId().equals(leader.getUniqueId())) {
                Logger.debug("&a" + leader.getName() + " &ehas just became leader of &a" + rank.getType().name());
                rank.setLeader(leader);
            } else {
                rank.updateSignLines();
            }
        }

        // update the scoreboards for rank group prefixes/icons
        updateScoreboardTeams();
    }

    public void updateScoreboardTeams() {
        for (Player online : Bukkit.getOnlinePlayers()) {
            Scoreboard scoreboard = online.getScoreboard();
            for (Rank rank : ranks) {
                Team team = scoreboard.getTeam(rank.getGroup());
                if (team == null) {
                    team = scoreboard.registerNewTeam(rank.getGroup());
                }
                //noinspection deprecation
                for (OfflinePlayer entry : new HashSet<>(team.getPlayers())) {
                    //noinspection deprecation
                    team.removePlayer(entry);
                }
                if (rank.getLeader() != null) {
                    //noinspection deprecation
                    team.addPlayer(rank.getLeader());
                }
                String prefix = VaultHook.getChat().getGroupPrefix((String) null, rank.getGroup());
                if (McMMORankRewards.getPlugin().hasPl3xIcons()) {
                    prefix = Pl3xIconsHook.translate(prefix);
                }
                if (prefix.length() > 16) {
                    prefix = prefix.substring(0, 15);
                }
                team.setPrefix(prefix);
            }
            Rank rank = getTopRank(online);
            if (rank == null) {
                continue;
            }
            //noinspection deprecation
            scoreboard.getTeam(rank.getGroup()).addPlayer(online);
        }
    }

    public Rank getRank(RankType type) {
        for (Rank rank : ranks) {
            if (rank.getType() == type) {
                return rank;
            }
        }
        return null;
    }

    public Rank getTopRank(Player player) {
        for (Rank rank : ranks) {
            if (rank.isLeader(player)) {
                return rank;
            }
        }
        return null;
    }
}
