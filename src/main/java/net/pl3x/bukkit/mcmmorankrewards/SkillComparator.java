package net.pl3x.bukkit.mcmmorankrewards;

import com.gmail.nossr50.datatypes.database.PlayerStat;

import java.util.Comparator;

public class SkillComparator implements Comparator<PlayerStat> {
    @Override
    public int compare(PlayerStat o1, PlayerStat o2) {
        return (o2.statVal - o1.statVal);
    }
}
