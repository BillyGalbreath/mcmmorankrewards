package net.pl3x.bukkit.mcmmorankrewards.configuration;

import net.pl3x.bukkit.mcmmorankrewards.McMMORankRewards;
import org.bukkit.configuration.file.FileConfiguration;

public class Config {
    public static boolean COLOR_LOGS;
    public static boolean DEBUG_MODE;
    public static String ANNOUNCE_LEADER_CHANGE;

    public static FileConfiguration getConfig() {
        return McMMORankRewards.getPlugin().getConfig();
    }

    public static void reload() {
        McMMORankRewards plugin = McMMORankRewards.getPlugin();

        plugin.saveDefaultConfig();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();

        COLOR_LOGS = config.getBoolean("color-logs", true);
        DEBUG_MODE = config.getBoolean("debug-mode", false);
        ANNOUNCE_LEADER_CHANGE = config.getString("announce-leader-change", "&7{player} &dhas just become leader of &7{group} &dby getting top rank in &7{skill}&d!");
    }
}
