package net.pl3x.bukkit.mcmmorankrewards.hook;

import net.pl3x.bukkit.pl3xicons.api.IconManager;

public class Pl3xIconsHook {
    public static String translate(String string) {
        return IconManager.getManager().translate(string);
    }
}
