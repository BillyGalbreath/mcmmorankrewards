package net.pl3x.bukkit.mcmmorankrewards.hook;

import net.pl3x.bukkit.mcmmorankrewards.McMMORankRewards;
import net.pl3x.bukkit.mcmmorankrewards.RankType;
import net.pl3x.bukkit.mcmmorankrewards.Ranks;
import net.pl3x.bukkit.pl3xsigns.SignData;
import net.pl3x.bukkit.pl3xsigns.event.LoadSignPacketEvent;
import net.pl3x.bukkit.pl3xsigns.event.UpdateSignPacketEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.List;

public class Pl3xSignsHook implements Listener {
    private McMMORankRewards plugin;

    public Pl3xSignsHook(McMMORankRewards plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onSignUpdatePacket(UpdateSignPacketEvent event) {
        String[] lines = event.getLines();
        if (!lines[0].isEmpty()) {
            String[] newLines = scanLines(lines);
            if (!lines[0].equals(newLines[0])) {
                event.setLines(newLines);
                event.setForceColor(true);
            }
        }
    }

    @EventHandler
    public void onSignLoadPacket(LoadSignPacketEvent event) {
        List<SignData> signDatas = event.getSignDatas();
        for (SignData signData : signDatas) {
            String[] lines = signData.getLines();
            if (!lines[0].isEmpty()) {
                String[] newLines = scanLines(lines);
                if (!lines[0].equals(newLines[0])) {
                    signData.setLines(newLines);
                    event.setForceColor(true);
                }
            }
        }
    }

    private String[] scanLines(String[] lines) {
        Ranks manager = plugin.getRanksManager();
        switch (lines[0]) {
            case "{acrobatics}":
                return manager.getRank(RankType.ACROBATICS).getSignLines();
            case "{alchemy}":
                return manager.getRank(RankType.ALCHEMY).getSignLines();
            case "{archery}":
                return manager.getRank(RankType.ARCHERY).getSignLines();
            case "{axes}":
                return manager.getRank(RankType.AXES).getSignLines();
            case "{excavation}":
                return manager.getRank(RankType.EXCAVATION).getSignLines();
            case "{fishing}":
                return manager.getRank(RankType.FISHING).getSignLines();
            case "{herbalism}":
                return manager.getRank(RankType.HERBALISM).getSignLines();
            case "{mining}":
                return manager.getRank(RankType.MINING).getSignLines();
            case "{power}":
                return manager.getRank(RankType.POWER).getSignLines();
            case "{repair}":
                return manager.getRank(RankType.REPAIR).getSignLines();
            case "{salvage}":
                return manager.getRank(RankType.SALVAGE).getSignLines();
            case "{smelting}":
                return manager.getRank(RankType.SMELTING).getSignLines();
            case "{swords}":
                return manager.getRank(RankType.SWORDS).getSignLines();
            case "{taming}":
                return manager.getRank(RankType.TAMING).getSignLines();
            case "{unarmed}":
                return manager.getRank(RankType.UNARMED).getSignLines();
            case "{woodcutting}":
                return manager.getRank(RankType.WOODCUTTING).getSignLines();
            default:
                return lines;
        }
    }
}
