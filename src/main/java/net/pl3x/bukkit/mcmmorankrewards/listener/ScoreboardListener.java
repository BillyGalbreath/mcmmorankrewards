package net.pl3x.bukkit.mcmmorankrewards.listener;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import net.pl3x.bukkit.mcmmorankrewards.McMMORankRewards;
import org.bukkit.Bukkit;

public class ScoreboardListener {
    private boolean listenerAlreadyAdded = false;

    public ScoreboardListener(McMMORankRewards plugin) {
        registerPacketListener(plugin);
    }

    private void registerPacketListener(McMMORankRewards plugin) {
        if (listenerAlreadyAdded) {
            return;
        }

        ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(plugin,
                ListenerPriority.NORMAL, PacketType.Play.Server.SCOREBOARD_DISPLAY_OBJECTIVE) {
            public void onPacketSending(PacketEvent event) {
                Bukkit.getScheduler().runTaskLater(plugin,
                        () -> McMMORankRewards.getPlugin().getRanksManager().updateScoreboardTeams(), 2);
            }
        });
        listenerAlreadyAdded = true;
    }
}
